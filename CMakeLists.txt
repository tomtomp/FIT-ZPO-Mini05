cmake_minimum_required(VERSION 2.8)
project(mt05)

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

# Set compiler flags.
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -pedantic -Wall -Werror")

add_executable(mt05 mt05.cpp)
target_link_libraries(mt05 ${OpenCV_LIBS})

